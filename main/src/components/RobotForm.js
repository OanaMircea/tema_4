import React, {Component} from 'react'


class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state = {
          id: '',
          type : '',
          name : '',
          mass : '',
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.add = () => {
          // alert('pressed')
          // console.log(this.state)
            this.props.onAdd({
                id: '',
                type : this.state.type,
                name: this.state.name,
                mass: this.state.mass
            })
        }

    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" id="name" onChange={this.handleChange} />
            <input type="text" placeholder="type" name="type" id="type" onChange={this.handleChange} />
            <input type="text" placeholder="mass" name="mass" id="mass" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.add} />  
        </div>
    }
}

export default RobotForm